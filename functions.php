<?php
require("Invoice.php");
require("LineItem.php");

const USERNAME = "root";
const PASSWORD = "password";
const URL = "mysql:host=127.0.0.1;dbname=todo";

function getInvoiceById($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select * from invoice_app.invoice i
        left join invoice_app.line_item li on li.invoice_id = i.id
        where i.id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();

    $invoice = null;
    foreach ($statement as $row) {
        if (isset($invoice)) {
            // lisa arve rida
            $lineItem = new LineItem();
            $lineItem->amount = $row["amount"];
            $lineItem->description = $row["description"];
            $lineItem->unitPrice = $row["unit_price"];
            $lineItem->vatRate = $row["vat"];

            $invoice->addLineItem($lineItem);
        } else {
            // tekitame invoice objekti
            // lisa arve rida
            $invoice = new Invoice();
            $invoice->id = $row["id"];
            $invoice->invoiceNumber = $row["invoice_number"];
            $invoice->date = $row["date"];
            $invoice->sellerName = $row["seller_name"];
            $invoice->sellerRegCode = $row["seller_reg_code"];
            $invoice->clientName = $row["client_name"];

            $lineItem = new LineItem();
            $lineItem->amount = $row["amount"];
            $lineItem->description = $row["description"];
            $lineItem->unitPrice = $row["unit_price"];
            $lineItem->vatRate = $row["vat"];

            $invoice->addLineItem($lineItem);
        }
    }

    return $invoice;
}

$invoice = getInvoiceById(1);

echo "Arve number: $invoice->invoiceNumber" . PHP_EOL;
echo "Kuupäev: $invoice->date" . PHP_EOL;
echo "Müüja: $invoice->sellerName" . PHP_EOL;
echo "Müüja reg. kood: $invoice->sellerRegCode" . PHP_EOL;
echo "Klient: $invoice->clientName" . PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;

echo "Kirjeldus, ühiku hind, kogus, km määr, summa enne km, km, summa koos km-ga";
foreach ($invoice->lineItems as $lineItem) {
    $line = "$lineItem->description, $lineItem->unitPrice, $lineItem->amount, $lineItem->vatRate, ";
    $line = $line . $lineItem->getPriceBeforeVat() . ", ";
    $line = $line . $lineItem->getVatAmount() . ", ";
    $line = $line . $lineItem->getTotalPrice() . ", ";

    echo $line . PHP_EOL;
}

echo PHP_EOL;
echo PHP_EOL;
echo "Summa enne km: " . $invoice->getPriceBeforeVat() . PHP_EOL;
echo "km: " . $invoice->getVatAmount() . PHP_EOL;
echo "Summa koos km-ga: " . $invoice->getTotalPrice();
