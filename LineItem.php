<?php

class LineItem {
    public $description;
    public $unitPrice;
    public $amount;
    public $vatRate;

    public function getPriceBeforeVat() {
        return $this->unitPrice * $this->amount;
    }

    public function getVatAmount() {
        return $this->getPriceBeforeVat() * $this->vatRate;
    }

    public function getTotalPrice() {
        return $this->getPriceBeforeVat() + $this->getVatAmount();
    }
}