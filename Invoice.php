<?php

class Invoice {
    public $id;
    public $invoiceNumber;
    public $date;
    public $sellerName;
    public $sellerRegCode;
    public $clientName;
    public $lineItems;

    public function __construct() {
        $this->lineItems = [];
    }

    public function addLineItem($lineItem) {
        $this->lineItems[] = $lineItem;
    }

    public function getPriceBeforeVat() {
        $sum = 0;
        foreach ($this->lineItems as $lineItem) {
            $sum += $lineItem->getPriceBeforeVat();
        }

        return $sum;
    }

    public function getVatAmount() {
        $sum = 0;
        foreach ($this->lineItems as $lineItem) {
            $sum += $lineItem->getVatAmount();
        }

        return $sum;
    }

    public function getTotalPrice() {
        return $this->getPriceBeforeVat() + $this->getVatAmount();
    }
}