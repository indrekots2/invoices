-- sql käsud tabelite tekitamiseks

create table invoice (
    id              int primary key auto_increment,
    invoice_number  varchar(255),
    date            date,
    seller_name     varchar(255),
    seller_reg_code varchar(255),
    client_name     varchar(255)
);

create table line_item (
    id          int primary key auto_increment,
    invoice_id  int,
    description varchar(255),
    unit_price  float,
    amount      float,
    vat         float
);
